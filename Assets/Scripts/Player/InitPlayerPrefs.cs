﻿using UnityEngine;
using System.Collections;

namespace LightsOff
{
	public class InitPlayerPrefs : MonoBehaviour
	{
		void Start ()
		{
			Application.targetFrameRate = 40;
			InGamePlayerPrefs.init ();
		}
	}
}