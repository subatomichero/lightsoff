using GooglePlayGames;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using System.Collections.Generic;
using System.Collections;
using System;

namespace LightsOff
{
	public class Player : MonoBehaviour
	{
		public AudioClip[] normalMovement;
		public AudioClip[] speedMovement;
		public AudioClip obstacleCrash;
		public AudioClip DemoStomp;

		private bool flopping, hasScoreBonus, canMoveNorth, canMoveSouth, canMoveWest, canMoveEast, stateHasChanged;
		private float speed, size, collectedLife, axisH = 0f, axisV = 0f;
		private LevelManager levelManager;
		private int x, y; // Use these to refer to elements of the arrays not positions
		private AudioSource audioSource;

		// Achievements
		private readonly string MOVEOUTTAMYWAY = "CgkIl4yxreAQEAIQAg";
		private readonly string SLOWDOWNTHEREBUDDY = "CgkIl4yxreAQEAIQAw";
		private readonly string TIMEWAITSFORNOMAN = "CgkIl4yxreAQEAIQBA";
		private readonly string IAMSOGREEDY = "CgkIl4yxreAQEAIQBQ";
		private readonly string TRAVELLER = "CgkIl4yxreAQEAIQCg";

		void Awake ()
		{
			this.levelManager = FindObjectOfType (typeof(LevelManager)) as LevelManager;
			this.levelManager.PlayerState = PlayerState.standard;
			this.axisH = 0f;
			this.axisV = 0f;
		}

		void Start ()
		{
			iTween.Init (this.gameObject);
			this.size = 1.0f;
			this.collectedLife = 5.0f;
			this.x = Mathf.RoundToInt (transform.position.x);
			UpdatePlayerPosition ();
			this.canMoveNorth = this.canMoveSouth = this.canMoveWest = this.canMoveEast = true;
			this.audioSource = (AudioSource)this.GetComponent (typeof(AudioSource));
		}
		
		// Update is called once per frame
		void Update ()
		{
			Vector2 move = new Vector2 (InputGetAxis ("Horizontal"), InputGetAxis ("Vertical"));
			if (this.cubeCanFlop (move))
				StartCoroutine (this.Flop (move));
			this.updateCubeState ();
		}

		private bool cubeCanFlop (Vector2 move)
		{
			bool value = (move.magnitude > 0.2f && 
				this.isOnGrid () && !this.flopping && 
				InGamePlayerPrefs.GetInt ("currentLightsOn") > 0 && 
				this.levelManager.LevelState == LevelState.levelIsPlaying);
			return value;
		}

		bool isOnGrid ()
		{
			if ((this.x < 0 || this.y < 0) || (this.x > this.levelManager.squareGridSize () - 1 ||
				this.y > this.levelManager.squareGridSize () - 1)) {
				return false;
			}
			return true;
		}

		void OnEnable ()
		{
			EventManager.StartListening ("PlayerStateChange", ResetPlayerState);
			EventManager.StartListening ("PlayerUp", MovePlayerUp);
			EventManager.StartListening ("PlayerDown", MovePlayerDown);
			EventManager.StartListening ("PlayerLeft", MovePlayerLeft);
			EventManager.StartListening ("PlayerRight", MovePlayerRight);
			EventManager.StartListening ("UpdatePlayerPosition", UpdatePlayerPosition);
		}

		void OnDisable ()
		{
			EventManager.StopListening ("PlayerStateChange", ResetPlayerState);
			EventManager.StopListening ("PlayerUp", MovePlayerUp);
			EventManager.StopListening ("PlayerDown", MovePlayerDown);
			EventManager.StopListening ("PlayerLeft", MovePlayerLeft);
			EventManager.StopListening ("PlayerRight", MovePlayerRight);
			EventManager.StopListening ("UpdatePlayerPosition", UpdatePlayerPosition);
		}

		void UpdatePlayerPosition ()
		{
			int currentGrid = InGamePlayerPrefs.GetInt ("currentGrid");
			this.x = Mathf.RoundToInt (transform.position.x);
			if (currentGrid > 0) {
				this.x -= (this.levelManager.squareGridSize () + this.levelManager.getGridSpacing () * currentGrid);
			}
			this.y = Mathf.RoundToInt (transform.position.z);
			this.levelManager.UnlockAchievement (TRAVELLER);
		}

		void ResetPlayerState ()
		{
			this.stateHasChanged = false;
			this.levelManager.PlayerState = PlayerState.standard;
			this.levelManager.PowerUpName = "";
		}

		void MovePlayerUp ()
		{
			switch (this.levelManager.RotateCount) {
			case 90:
			case -270:
				this.moveCube (1f, "Horizontal");
				break;
			case 180:
			case -180:
				this.moveCube (-1f, "Vertical");
				break;
			case 270:
			case -90:
				this.moveCube (-1f, "Horizontal");
				break;
			default:
				this.moveCube (1f, "Vertical");
				break;
			}
		}

		void MovePlayerDown ()
		{
			switch (this.levelManager.RotateCount) {
			case 90:
			case -270:
				this.moveCube (-1f, "Horizontal");
				break;
			case 180:
			case -180:
				this.moveCube (1f, "Vertical");
				break;
			case 270:
			case -90:
				this.moveCube (1f, "Horizontal");
				break;
			default:
				this.moveCube (-1f, "Vertical");
				break;
			}
		}

		void MovePlayerLeft ()
		{
			switch (this.levelManager.RotateCount) {
			case 90:
			case -270:
				this.moveCube (1f, "Vertical");
				break;
			case 180:
			case -180:
				this.moveCube (1f, "Horizontal");
				break;
			case 270:
			case -90:
				this.moveCube (-1f, "Vertical");
				break;
			default:
				this.moveCube (-1f, "Horizontal");
				break;
			}
		}

		void MovePlayerRight ()
		{
			switch (this.levelManager.RotateCount) {
			case 90:
			case -270:
				this.moveCube (-1f, "Vertical");
				break;
			case 180:
			case -180:
				this.moveCube (-1f, "Horizontal");
				break;
			case 270:
			case -90:
				this.moveCube (1f, "Vertical");
				break;
			default:
				this.moveCube (1f, "Horizontal");
				break;
			}
		}

		void moveCube (float val, String dir)
		{
			if ("Horizontal".Equals (dir)) {
				this.axisH = val;
			} else {
				this.axisV = val;
			}
		}

		private void updateCubeState ()
		{
			switch (this.levelManager.PlayerState) {
			case PlayerState.standard:
				if (!this.stateHasChanged) { // make sure to change this back to false when the player needs to change states
					this.stateHasChanged = true;
					this.collectedLife = 5.0f;
					this.speed = 2.5f;
					this.hasScoreBonus = false;
					this.levelManager.PlayerHasTimeBonus = false;
				}
				break;
			case PlayerState.hasSpeed:
				if (!this.stateHasChanged) {
					this.stateHasChanged = true;
					this.speed = 4.0f;
					this.reducePowerUpLife ();
				}
				break;
			case PlayerState.hasDemo:
				if (!this.stateHasChanged) {
					this.stateHasChanged = true;
					this.reducePowerUpLife ();
				}
				break;
			case PlayerState.hasScore:
				if (!this.stateHasChanged) {
					this.stateHasChanged = true;
					this.reducePowerUpLife ();
					this.hasScoreBonus = true;
				}
				break;
			case PlayerState.hasTime:
				if (!this.stateHasChanged) {
					this.stateHasChanged = true;
					this.reducePowerUpLife ();
					this.levelManager.PlayerHasTimeBonus = true;
				}
				break;
			}
		}

		private float InputGetAxis (String axis)
		{
			var v = Input.GetAxis (axis);
			if (Mathf.Abs (v) > 0.005f) {
				return v;
			} else if (axis == "Horizontal") {
				return axisH;
			} else if (axis == "Vertical") {
				return axisV;
			} else {
				return 0f;
			}
		}

		private void reducePowerUpLife ()
		{
			this.collectedLife -= Time.deltaTime;
			if (this.collectedLife <= 0f)
				this.levelManager.PlayerState = PlayerState.standard;	
		}
		
		/// plays the relevant sfx of the cube
		public void playSFX ()
		{
			// lets control the sounds to play using the state machine
			switch (this.levelManager.PlayerState) {
			case PlayerState.standard:
			case PlayerState.hasScore:
			case PlayerState.hasTime:
				// just play one of the three standard movement sounds
				this.audioSource.volume = 5f;
				this.audioSource.clip = this.normalMovement [UnityEngine.Random.Range (0, this.normalMovement.Length)];
				break;
			case PlayerState.hasSpeed:
				// play element 3 in the audio bank for every move of the cube
				this.audioSource.clip = speedMovement [UnityEngine.Random.Range (0, this.speedMovement.Length)];
				break;
			case PlayerState.hasDemo:
				// play element 6
				this.audioSource.clip = this.DemoStomp;
				this.audioSource.volume = 8f;
				break;
			default:
				// if we dont know whats happening we are in the standard state
				this.levelManager.PlayerState = PlayerState.standard;
				break;
			}
			this.audioSource.Play ();
		}

		/// This function checks if the cube is on a lights position and manages that light
		private void checkCubeOnLight ()
		{
			int gridSize = this.levelManager.squareGridSize ();
			if (this.x >= 0 && this.y >= 0 && this.x < gridSize && this.y < gridSize) {
				int currentGrid = InGamePlayerPrefs.GetInt ("currentGrid");
				int currentScore = InGamePlayerPrefs.GetInt ("currentScore");
				gridtile[,] level = this.levelManager.GridLevels [currentGrid] ["GRID"] as gridtile[,];
				this.cubeCanMove (level);
				if (level [this.x, this.y].hasLightsOn ()) {
					level [this.x, this.y].turnLightOff ();
					InGamePlayerPrefs.SetInt ("currentScore", currentScore += (this.hasScoreBonus ? 10 : 5));
				} else if (!level [this.x, this.y].hasLightsOn () && !level [this.x, this.y].isOccupiedByObject ()
					&& !level [this.x, this.y].HasPortal) {
					level [this.x, this.y].turnLightOn ();
					if (!this.hasScoreBonus)
						InGamePlayerPrefs.SetInt ("currentScore", currentScore -= 5);
				}
			} else {
				// Player has fallen off the grid
				EventManager.TriggerEvent ("KillPlayer");
			}
			EventManager.TriggerEvent ("ScoreUpdate");
		}

		private void cubeCanMove (gridtile[,] level)
		{
			if (this.levelManager.PlayerState != PlayerState.hasDemo) {
				this.canMoveNorth = this.tileIsOccupied (level [this.x, this.y].getNorthTile ()) ? false : true;
				this.canMoveSouth = this.tileIsOccupied (level [this.x, this.y].getSouthTile ()) ? false : true;
				this.canMoveWest = this.tileIsOccupied (level [this.x, this.y].getWestTile ()) ? false : true;
				this.canMoveEast = this.tileIsOccupied (level [this.x, this.y].getEastTile ()) ? false : true;
			} else {
				this.canMoveNorth = this.canMoveSouth = this.canMoveWest = this.canMoveEast = true;
			}
		}

		private bool tileIsOccupied (gridtile tile)
		{
			return tile != null && tile.isOccupiedByObject ();
		}

		private IEnumerator Flop (Vector2 movDir)
		{
			Vector3 pivot = new Vector3 (0, 0, 0);
			Vector3 dir = new Vector3 (0, 0, 0);
			
			if (this.flopping)
				yield break; // ignore other commands while flopping
			
			if (movDir.y > 0 && this.canMoveNorth) { 
				dir = Vector3.forward; // will flop forward
				pivot = new Vector3 (0, -1, 1); // defines point around which rotate
				this.y++;
				this.flopping = true;
			}
			
			if (movDir.y < 0 && this.canMoveSouth) {
				dir = -Vector3.forward;
				pivot = new Vector3 (0, -1, -1);
				this.y--;
				this.flopping = true;
			}
			
			if (movDir.x < 0 && this.canMoveWest) {
				dir = -Vector3.right;
				pivot = new Vector3 (-1, -1, 0);
				this.x--;
				this.flopping = true;
			}
			
			if (movDir.x > 0 && this.canMoveEast) {
				dir = Vector3.right;
				pivot = new Vector3 (1, -1, 0);
				this.x++;
				this.flopping = true;
			}

			this.AlignBlock (); 
			// calculates the point around which the block will flop
			if (this.flopping) {
				pivot = transform.position + (pivot * size / 2);
				var org = transform.position - pivot;
				var dest = (transform.position + dir * size) - pivot;
				var rot0 = transform.rotation;
				var rot1 = Quaternion.FromToRotation (org, dest) * rot0; 
				
				float a = 0;
				while (a < 1) {
					var dt = Time.deltaTime * speed;
					a += dt;
					transform.position = Vector3.Slerp (org, dest, a) + pivot;
					transform.rotation = Quaternion.Lerp (rot0, rot1, a);
					yield return null;
				}
				if (InGamePlayerPrefs.GetInt ("currentLightsOn") > 0)
					this.checkCubeOnLight ();
				this.flopping = false;
				this.playSFX ();
				this.levelManager.PlayerPosition = new Vector2 (x, y);
				if (this.levelManager.PlayerState == PlayerState.hasDemo)
					EventManager.TriggerEvent ("CameraShake");
			}
		}

		void OnGUI ()
		{
			this.axisH = 0f;
			this.axisV = 0f;
		}

		private void AlignBlock ()
		{
			var angles = transform.eulerAngles;
			// forces euler angles to be multiple of 90
			angles.x = 90 * Mathf.RoundToInt (angles.x / 90);
			angles.y = 90 * Mathf.RoundToInt (angles.y / 90);
			angles.z = 90 * Mathf.RoundToInt (angles.z / 90);
			transform.eulerAngles = angles;
			var pos = transform.position;
			// forces x and z to be in a grid 
			pos.x = size * Mathf.RoundToInt (pos.x / size);
			pos.z = size * Mathf.RoundToInt (pos.z / size);
			if (!GetComponent<Rigidbody> ())
				pos.y = size * Mathf.RoundToInt (pos.y / size);
			transform.position = pos;
		}

		void OnCollisionEnter (Collision other)
		{
			if ("Wall".Equals (other.transform.tag)) {
				// we hit the wall when we have the demo bonus, destroy the wall
				GetComponent<AudioSource> ().PlayOneShot (this.obstacleCrash);
				GameObject obj = ObjectPooler.GetPooledExplosion ();
				obj.transform.position = other.transform.position;
				obj.SetActive (true);
				Handheld.Vibrate ();
				other.gameObject.SetActive (false);

				// increment achievement
				int count = this.levelManager.ObstaclesDestroyed;
				count++;
				this.levelManager.ObstaclesDestroyed = count;

				// set that part of the grid as unoccupied
				int currentGrid = InGamePlayerPrefs.GetInt ("currentGrid");
				gridtile[,] level = this.levelManager.GridLevels [currentGrid] ["GRID"] as gridtile[,];
				level [this.x, (int)other.transform.position.z].setIsOccupied (false);
				this.levelManager.GridLevels [currentGrid] ["GRID"] = level;

				// add 100 onto the score
				int currentScore = InGamePlayerPrefs.GetInt ("currentScore");
				InGamePlayerPrefs.SetInt ("currentScore", currentScore += 100);
				EventManager.TriggerEvent ("ScoreUpdate");
			}
		}

		void OnTriggerEnter (Collider other)
		{
			if ("PowerUp".Equals (other.transform.tag)) {
				this.stateHasChanged = false;
				this.levelManager.PowerUpPickedup = true;
				this.levelManager.PowerUpName = other.transform.name;

				switch (other.transform.name) {
				case "Demolition Bonus":
					this.levelManager.UnlockAchievement (MOVEOUTTAMYWAY);
					this.levelManager.PlayerState = PlayerState.hasDemo;
					break;
				case "Score Bonus":
					this.levelManager.UnlockAchievement (IAMSOGREEDY);
					this.levelManager.PlayerState = PlayerState.hasScore;
					break;
				case "Time Bonus":
					this.levelManager.UnlockAchievement (TIMEWAITSFORNOMAN);
					this.levelManager.PlayerState = PlayerState.hasTime;
					break;
				case "Speed Bonus":
					this.levelManager.UnlockAchievement (SLOWDOWNTHEREBUDDY);
					this.levelManager.PlayerState = PlayerState.hasSpeed;
					break;
				}
			}
		}
	}
}