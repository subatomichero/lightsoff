using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace LightsOff
{
	public class MusicPlayer : MonoBehaviour
	{
		public AudioClip mainMenuMusic;	// Main Menu music
		public AudioClip[] soundTracks;		// Level music
		
		private static bool isCreated = false;
		private int tracknumber;
		private MusicState musicState;
		private AudioSource audioSource;
		
		void Awake ()
		{
			if (!isCreated) {
				// this is the first instance, make it persist
				DontDestroyOnLoad (this.gameObject);
				isCreated = true;
			} else {
				// this is the second, destroy it
				Destroy (this.gameObject);
			}
		}
		
		void Start ()
		{
			this.initJukeBox ();
		}

		private void initJukeBox ()
		{
			this.musicState = MusicState.inMainMenu;
			this.audioSource = (AudioSource)GetComponent (typeof(AudioSource));

			if (Time.timeScale != 1)
				Time.timeScale = 1;

			// Check that the user hasnt already set a volume
			float volume = PlayerPrefs.GetFloat ("volume");
			if (volume == 0.0f) {
				// it hasnt been set
				volume = 0.5f;
				PlayerPrefs.SetFloat ("volume", volume);
			}
			if (AudioListener.volume == 0)
				AudioListener.volume = volume;

			this.tracknumber = Random.Range (0, this.soundTracks.Length);
		}
		
		public void setMusicState (MusicState state)
		{
			this.musicState = state;	
		}
		
		public MusicState getMusicState ()
		{
			return this.musicState;	
		}

		public AudioSource getAudio ()
		{
			return this.audioSource;
		}

		void PlayMenuMusic ()
		{
			this.audioSource.clip = this.mainMenuMusic;
			this.audioSource.Play ();
			this.audioSource.loop = true;
		}

		void Update ()
		{
			switch (this.musicState) {
			case MusicState.inMainMenu:
				// WE have to check if the audio is playing otherwise it continually tries to load and play the same clip
				if (this.gameObject.transform.position != GameObject.FindGameObjectWithTag ("MainCamera").transform.position) {
					this.gameObject.transform.position = GameObject.FindGameObjectWithTag ("MainCamera").transform.position;
				}
				if (!this.audioSource.isPlaying && this.audioSource.clip != this.mainMenuMusic) {
					this.PlayMenuMusic ();
				}
				break;
			case MusicState.inGame:
				if (this.gameObject.transform.position != GameObject.FindGameObjectWithTag ("MainCamera").transform.position) {
					this.gameObject.transform.position = GameObject.FindGameObjectWithTag ("MainCamera").transform.position;
				}
				if (this.audioSource.isPlaying && this.audioSource.clip == this.mainMenuMusic) {
					this.audioSource.Stop ();
					this.audioSource.loop = false;
					this.audioSource.clip = this.soundTracks [this.tracknumber];
					this.audioSource.Play ();
					StartCoroutine (this.playNextTrack ());
				}
				break;
			}
		}
		
		private IEnumerator playNextTrack ()
		{
			// this function waits until the current track is finished then plays the next track in the list
			yield return new WaitForSeconds (this.audioSource.clip.length + 3f);
			this.gotoNextTrack ();
		}

		private void gotoNextTrack ()
		{
			this.tracknumber++;
			if (this.atTheEndOfSongs ())
				this.tracknumber = 0;
			this.audioSource.clip = this.soundTracks [this.tracknumber];
			this.audioSource.Play ();
			StartCoroutine (this.playNextTrack ());
		}

		private bool atTheEndOfSongs ()
		{
			return (this.tracknumber >= this.soundTracks.Length);
		}
	}
}