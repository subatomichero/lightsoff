﻿using UnityEngine;
using System.Collections;

namespace LightsOff
{
	public class skyBoxController : MonoBehaviour
	{
		public Material[] skyBoxes;
		
		void Start ()
		{
			int random = Random.Range (0, this.skyBoxes.Length);
			RenderSettings.skybox = skyBoxes [random];
		}
	}
}