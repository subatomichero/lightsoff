﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace LightsOff
{
	public class MainMenu : MonoBehaviour
	{	
		public MusicPlayer musicPlayer;
		public Canvas QuitMenu;
		public Text SignInText;
		public Text Achievements;
		public Text Leaderboard;
		private Canvas StartMenu;

		void Start ()
		{
			this.StartMenu = (Canvas)this.gameObject.GetComponent (typeof(Canvas));
			this.QuitMenu = (Canvas)this.QuitMenu.GetComponent (typeof(Canvas));
			this.QuitMenu.enabled = false;
			this.musicPlayer.setMusicState (MusicState.inMainMenu);
			Social.localUser.Authenticate ((bool success) => {
				this.Achievements.gameObject.SetActive (success);
				this.Leaderboard.gameObject.SetActive (success);
				this.SignInText.text = (success ? "Sign Out" : "Sign In");
			});
		}

		void Update ()
		{
			if (Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown (KeyCode.Menu)) {
				if (this.StartMenu.enabled) {
					this.SwitchMenus ();
				} else if (this.QuitMenu.enabled) {
					YesPress ();
				}
			}
		}

		public void SignInOrOut ()
		{
			if (this.SignInText.text == "Sign Out") {
				PlayGamesPlatform.Instance.SignOut ();
				this.SignInText.text = "Sign In";
				this.Achievements.gameObject.SetActive (false);
				this.Leaderboard.gameObject.SetActive (false);
			} else {
				Social.localUser.Authenticate ((bool success) => {
					this.Achievements.gameObject.SetActive (success);
					this.Leaderboard.gameObject.SetActive (success);
					this.SignInText.text = (success ? "Sign Out" : "Sign In");
				});
			}
		}

		public void StartGame ()
		{
			StartCoroutine (go ());
		}

		IEnumerator go ()
		{
			yield return new WaitForSeconds (1f);
			this.musicPlayer.setMusicState (MusicState.inGame);
			Application.LoadLevel ("levelLoader");
		}

		public void ExitGame ()
		{
			this.SwitchMenus ();
		}

		public void YesPress ()
		{
			Application.Quit ();
		}

		public void NoPress ()
		{
			this.SwitchMenus ();
		}

		private void SwitchMenus ()
		{
			this.StartMenu.enabled = !this.StartMenu.enabled;
			this.QuitMenu.enabled = !this.QuitMenu.enabled;
		}

		public void ShowAchievements ()
		{
			Social.ShowAchievementsUI ();
		}

		public void ShowLeaderboard ()
		{
			Social.ShowLeaderboardUI ();
		}
	}
}