using UnityEngine;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

namespace LightsOff
{
	public class SplashScreen : MonoBehaviour
	{
		IEnumerator Start ()
		{
			PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder ()
				.RequireGooglePlus ()
				.Build ();
			PlayGamesPlatform.InitializeInstance (config);
			PlayGamesPlatform.Activate ();
			((PlayGamesPlatform)Social.Active).SetDefaultLeaderboardForUI ("CgkIl4yxreAQEAIQAA");

			AdBuddizBinding.SetAndroidPublisherKey ("e6f13ce1-c1aa-42e1-97d1-f4c591470df1");
			AdBuddizBinding.CacheAds ();

			float audioLength = GetComponent<AudioSource> ().clip.length;
			yield return new WaitForSeconds (audioLength + 0.5f);
			int eula = PlayerPrefs.GetInt ("EULA");
			if (eula == 1)
				Application.LoadLevel ("mainMenu");
			else 
				Application.LoadLevel ("eula");
		}
	}
}