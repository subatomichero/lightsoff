using UnityEngine;
using System.Collections;

namespace LightsOff
{
	public class CameraMover : MonoBehaviour
	{
		private Hashtable rotateClock, rotateAntiClock, shakeCamera, moveToGrid;
		private int rotateCount = 0;
		public LevelManager levelManager;

		void Start ()
		{
			iTween.Init (this.gameObject);
			float size = (this.levelManager.squareGridSize () * 0.5f) - 0.5f;
			if (this.levelManager.squareGridSize () == 3)
				size = 1;
			this.transform.position = new Vector3 (size, 1, size);
			this.initHashTables ();
		}

		void OnEnable ()
		{
			EventManager.StartListening ("CameraShake", shakeTheCamera);
			EventManager.StartListening ("RotateLeft", rotateLeft);
			EventManager.StartListening ("RotateRight", rotateRight);
			EventManager.StartListening ("UpdateCameraPosition", updateCameraPosition);
		}

		void OnDisable ()
		{
			EventManager.StopListening ("CameraShake", shakeTheCamera);
			EventManager.StopListening ("RotateLeft", rotateLeft);
			EventManager.StopListening ("RotateRight", rotateRight);
			EventManager.StopListening ("UpdateCameraPosition", updateCameraPosition);
		}

		void shakeTheCamera ()
		{
			iTween.ShakePosition (this.gameObject, this.shakeCamera);
		}

		void updateCameraPosition ()
		{
			int currentGrid = InGamePlayerPrefs.GetInt ("currentGrid");
			this.transform.position = this.levelManager.GridCentres [currentGrid];
		}

		private void initHashTables ()
		{
			// Initialise our rotate clockwise hashtable
			this.rotateClock = new Hashtable ();
			this.rotateClock.Add ("name", "rotateRight");
			this.rotateClock.Add ("amount", new Vector3 (0, 0.25f, 0));
			this.rotateClock.Add ("time", 0.75f);
			this.rotateClock.Add ("easetype", iTween.EaseType.easeInOutSine);
			this.rotateClock.Add ("oncomplete", "ClockUpdate");
			
			// Initialise our rotate anti clockwise hashtable
			this.rotateAntiClock = new Hashtable ();
			this.rotateAntiClock.Add ("name", "rotateLeft");
			this.rotateAntiClock.Add ("amount", new Vector3 (0, -0.25f, 0));
			this.rotateAntiClock.Add ("time", 0.75f);
			this.rotateAntiClock.Add ("easetype", iTween.EaseType.easeInOutSine);
			this.rotateAntiClock.Add ("oncomplete", "AntiClockUpdate");
			
			// initialise our camera shake effect playable when the player has demolition bonus
			this.shakeCamera = new Hashtable ();
			this.shakeCamera.Add ("name", "shakeCamera");
			this.shakeCamera.Add ("amount", new Vector3 (0.15f, 0.2f, 0));
			this.shakeCamera.Add ("time", 0.35f);
			
			// initialise the movetogrid hashtable
			this.moveToGrid = new Hashtable ();
			this.moveToGrid.Add ("name", "moveToGrid");
		}

		void ClockUpdate ()
		{
			this.rotateCount += 90;
			if (this.rotateCount == 450) {
				this.rotateCount = 90;	
			}
			this.levelManager.RotateCount = this.rotateCount;
		}

		void AntiClockUpdate ()
		{
			this.rotateCount -= 90;
			if (this.rotateCount == -450) {
				this.rotateCount = -90;	
			}
			this.levelManager.RotateCount = this.rotateCount;
		}

		void rotateRight ()
		{
			iTween.RotateBy (this.gameObject, this.rotateClock);
		}

		void rotateLeft ()
		{
			iTween.RotateBy (this.gameObject, this.rotateAntiClock);
		}
	}
}