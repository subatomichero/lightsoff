using UnityEngine;
using System.Collections;

namespace LightsOff
{
	public class CameraManager : MonoBehaviour
	{
		void Start ()
		{
			this.transform.position = new Vector3 (this.transform.parent.position.x, 3.75f, -5.5f);
			this.transform.eulerAngles = new Vector3 (28, 0, 0);
		}
	}
}