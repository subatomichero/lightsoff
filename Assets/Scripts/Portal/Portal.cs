﻿using GooglePlayGames;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using System.Collections;

namespace LightsOff
{
	public class Portal : MonoBehaviour
	{
		private GameObject nextPortal;
		private GameObject prevPortal;
		private AudioSource audioSource;

		void Start ()
		{
			this.audioSource = (AudioSource)this.GetComponent (typeof(AudioSource));
		}

		public GameObject NextPortal {
			get { return this.nextPortal; }
			set { this.nextPortal = value; }
		}

		public GameObject PrevPortal {
			get { return this.prevPortal; }
			set { this.prevPortal = value; }
		}

		IEnumerator OnTriggerEnter (Collider other)
		{
			if (other.transform.tag == "Player") {
				bool canTeleport = InGamePlayerPrefs.GetInt ("canTeleport") == 1;
				if (canTeleport) {
					yield return new WaitForSeconds (0.5f);
					this.audioSource.Play ();
					Vector3 position;
					int currentGrid = InGamePlayerPrefs.GetInt ("currentGrid");
					if (this.NextPortal != null) {
						position = NextPortal.transform.position;
						other.transform.position = new Vector3 (position.x, -0.5f, position.z);
						InGamePlayerPrefs.SetInt ("currentGrid", ++currentGrid);
					} else if (this.PrevPortal != null) {
						position = PrevPortal.transform.position;
						other.transform.position = new Vector3 (position.x, -0.5f, position.z);
						InGamePlayerPrefs.SetInt ("currentGrid", --currentGrid);
					}
					InGamePlayerPrefs.SetInt ("canTeleport", 0);
					EventManager.TriggerEvent ("UpdateCameraPosition");
					EventManager.TriggerEvent ("UpdatePlayerPosition");
				}
			}
		}

		IEnumerator OnTriggerExit (Collider other)
		{
			if (other.transform.tag == "Player") {
				yield return new WaitForSeconds (0.25f);
				InGamePlayerPrefs.SetInt ("canTeleport", 1);
			}
		}
	}
}