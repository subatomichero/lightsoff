﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace LightsOff
{
	public class PortalManager : MonoBehaviour
	{
		public LevelManager levelManager;
		public GameObject parent;

		private GameObject clone;
		private SortedList<int, GameObject> portals;
		private int totalPortalCount;

		void Start ()
		{
			totalPortalCount = 0;
			this.gameObject.SetActive (true);
		}

		void OnEnable ()
		{
			EventManager.StartListening ("BuildPortals", SpawnPortals);
		}

		void OnDisable ()
		{
			EventManager.StopListening ("BuildPortals", SpawnPortals);
		}

		void SpawnPortals ()
		{
			// for evey grid we spawn a portal. We spawn 1 portal on the first and last grid, two otherwise
			// The first portal has no previous portal and the last portal has no next portal
			int currentGridCount = InGamePlayerPrefs.GetInt ("currentGridCount");
			this.portals = new SortedList<int, GameObject> ();

			// For every grid (e.g if we had 2 grids on screen - 0,1)
			for (int i = 0; i < currentGridCount; i++) {
				int portalCount = 0;
				Dictionary<string, object> gridLevel = this.levelManager.GridLevels [i];
				SpawnPortalOnGrid (gridLevel, portalCount, currentGridCount);
			}

			SetupPortalLinks ();
			this.levelManager.CreationState = CreationState.portalsHaveSpawned;
		}

		void SpawnPortalOnGrid (Dictionary<string, object> level, int portalCount, int currentGridCount)
		{
			Vector2[] gridLocations = this.levelManager.GridLocations;
			int levelNumber = (int)level ["LEVEL"];
			gridtile[,] gridLevel = level ["GRID"] as gridtile[,];
			int gridSize = this.levelManager.squareGridSize ();
			short startCol = (short)gridLocations [levelNumber].x;
			short startRow = (short)gridLocations [levelNumber].y;
			int endCol = startCol + gridSize;
			int endRow = startRow + gridSize;

			int gridColumn = 0;
			for (int col = startCol; col < endCol; col++) {
				int gridRow = 0;
				for (int row = startRow; row < endRow; row++) {
					if (!gridLevel [gridColumn, gridRow].isOccupiedByObject () && gridLevel [gridColumn, gridRow].getIsCornerSpace ()) {
						// we can spawn a portal here, if this is the first or last grid only spawn once
						gridLevel [gridColumn, gridRow].HasPortal = true;
						gridLevel [gridColumn, gridRow].turnLightOff ();
						this.clone = ObjectPooler.GetPooledPortal ();
						this.clone.transform.position = new Vector3 (col, 0, row);
						this.clone.SetActive (true);
						this.clone.transform.parent = this.parent.transform;
						this.portals.Add (totalPortalCount++, this.clone);
						if (portalCount == 2 || levelNumber == 0 || levelNumber == (currentGridCount - 1))
							return;
					}
					gridRow++;
				}
				gridColumn++;
			}
			this.levelManager.GridLevels [levelNumber] ["GRID"] = gridLevel;
		}
		
		void SetupPortalLinks ()
		{
			// Setup the links
			for (int i = 0; i < this.portals.Count; i++) {
				if (i == 0) {
					CreateNextLink (i);
				} else if (i == this.portals.Count - 1) {
					CreatePreviousLink (i);
				} else {
					if (i % 2 != 0) {
						CreatePreviousLink (i);
					} else {
						CreateNextLink (i);
					}
				}
			}
			this.gameObject.SetActive (false);
		}

		void CreateNextLink (int i)
		{
			GameObject next = this.portals.Values [i + 1];
			this.portals.Values [i].GetComponent<Portal> ().NextPortal = next;
		}

		void CreatePreviousLink (int i)
		{
			GameObject prev = this.portals.Values [i - 1];
			this.portals.Values [i].GetComponent<Portal> ().PrevPortal = prev;
		}
	}
}