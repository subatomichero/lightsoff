using UnityEngine;
using System.Collections;

namespace LightsOff
{
	public class ScrollingGUIText : MonoBehaviour
	{
		public GUIText[] textElements;
		public float scrollTime;
		public float scrollSpeed;
		public float currentTime = 0.0f;
		
		void Update ()
		{
			this.currentTime += Time.deltaTime;
			this.scrollTime -= Time.deltaTime;
			
			if (this.scrollTime < 0) {
				foreach (GUIText text in textElements) {
					text.transform.Translate (Vector3.up * this.scrollSpeed);
				}
			}
			
			if ((Input.GetKeyDown (KeyCode.Escape) || Input.GetMouseButton (0))
				|| (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved)
				|| currentTime > 22f) {
				InGamePlayerPrefs.init ();
				Application.LoadLevel ("mainMenu");
			}
		}
	}
}