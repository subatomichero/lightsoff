﻿using UnityEngine;
using System.Collections;

namespace LightsOff
{
	public class ObjectRotator : MonoBehaviour
	{
		private Vector3 position;	
		void Awake ()
		{
			this.position = this.gameObject.transform.localPosition;
			iTween.Init (this.gameObject);
		}
		
		void LateUpdate ()
		{
			iTween.RotateUpdate (this.gameObject, new Vector3 (this.position.x, this.position.y += 0.02f, this.position.z), 0.03f);
		}
	}
}