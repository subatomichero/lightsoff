using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace LightsOff
{
	public class PlayerSpawner : MonoBehaviour
	{
		public GameObject Player;
		public LevelManager levelManager;
		public GameObject parent;

		private Dictionary<int, Vector3[]> respawns;
		private Quaternion startRot;
		private int spawnPoint;
		private GameObject clone;
		
		void Awake ()
		{ 
			this.respawns = new Dictionary<int, Vector3[]> ();
		}

		void OnEnable ()
		{
			EventManager.StartListening ("SpawnPlayer", SpawnPlayer);
			EventManager.StartListening ("KillPlayer", Kill);
			EventManager.StartListening ("PlayerOutOfTime", Poot);
		}

		void OnDisable ()
		{
			EventManager.StopListening ("SpawnPlayer", SpawnPlayer);
			EventManager.StopListening ("KillPlayer", Kill);
			EventManager.StopListening ("PlayerOutOfTime", Poot);
		}

		void Poot ()
		{
			StartCoroutine (PlayerOutOfTime ());
		}

		IEnumerator PlayerOutOfTime ()
		{
			this.clone.GetComponentInChildren<Rigidbody> ().useGravity = false;
			iTween.MoveAdd (clone, new Vector3 (0, 1.5f, 0), 1f);
			yield return new WaitForSeconds (1f);
			iTween.ShakePosition (clone, new Vector3 (0.15f, 0.2f, 0), 1f);
			yield return new WaitForSeconds (0.5f);
			GameObject obj = ObjectPooler.GetPooledExplosion ();
			Vector2 pos = this.levelManager.PlayerPosition;
			obj.transform.position = new Vector3 (pos.x, 1.5f, pos.y);
			obj.SetActive (true);
			Handheld.Vibrate ();
			Destroy (clone);
			yield return new WaitForSeconds (2f);
			int currentLives = InGamePlayerPrefs.GetInt ("currentLives");
			if (currentLives > 0) {
				InGamePlayerPrefs.SetInt ("currentLives", --currentLives);
				EventManager.TriggerEvent ("UpdateTime");
				EventManager.TriggerEvent ("ScoreUpdate");
				SpawnPlayer ();
			} else {
				EventManager.TriggerEvent ("GameOverUi");
			}
		}

		void Kill ()
		{
			StartCoroutine (KillPlayer ());
		}

		IEnumerator KillPlayer ()
		{
			yield return new WaitForSeconds (2.5f);
			int currentLives = InGamePlayerPrefs.GetInt ("currentLives");
			if (currentLives > 0) {
				// Destroy the player, wait a few seconds then respawn it
				InGamePlayerPrefs.SetInt ("currentLives", --currentLives);
				this.levelManager.PlayerIsAlive = false;
				EventManager.TriggerEvent ("ScoreUpdate");
				SpawnPlayer ();
			} else {
				// the player doesnt have any lives left, game over
				// enter name into high score
				this.levelManager.PlayerIsAlive = false;
				
				// Show the game over ui with name prompt
				this.levelManager.level.SetActive (false);
				EventManager.TriggerEvent ("GameOverUi");
			}
		}

		Vector3 CalculateSpawnPoint (int currentGrid)
		{
			Vector3 spawnPoint = this.GetSpawnPoint ();
			if (currentGrid > 0)
				spawnPoint.x += (this.levelManager.squareGridSize () + this.levelManager.getGridSpacing ()) * currentGrid;
			return spawnPoint;
		}

		Vector3 CalculateSpawnPoint (int currentGrid, gridtile[,] gridLevel)
		{
			int first = Random.Range (0, this.levelManager.squareGridSize ());
			int second = Random.Range (0, this.levelManager.squareGridSize ());
			Vector3 spawnPoint = gridLevel [first, second].getGridTilePosition ();
			if (currentGrid > 0)
				spawnPoint.x += (this.levelManager.squareGridSize () + this.levelManager.getGridSpacing ()) * currentGrid;
			return spawnPoint;
		}
		                          
		void SpawnPlayer ()
		{
			if (this.clone)
				Destroy (this.clone);
			if (this.respawns.Count == 0) {
				this.createSpawnPoints ();
				EventManager.TriggerEvent ("StartCountdown");
			}
			int currentGrid = InGamePlayerPrefs.GetInt ("currentGrid");
			Vector3 spawnPoint = CalculateSpawnPoint (currentGrid);
			float x = spawnPoint.x;
			gridtile[,] gridLevel = this.levelManager.GridLevels [currentGrid] ["GRID"] as gridtile[,];
			int attempts = 0;
			while (gridLevel[(int) x, (int) spawnPoint.z].HasPortal || gridLevel[(int) x, (int) spawnPoint.z].isOccupiedByObject()
			       || this.playerIsBlocked(gridLevel[(int)x, (int)spawnPoint.z])) {
				spawnPoint = CalculateSpawnPoint (currentGrid);
				x = spawnPoint.x;
				attempts++;
				if (attempts > 4)
					break;
			}

			// if we had more than 4 attempts, just put the player anywhere on the grid
			if (attempts > 4) {
				spawnPoint = CalculateSpawnPoint (currentGrid, gridLevel);
				int x1 = (int)spawnPoint.x - (this.levelManager.squareGridSize () + this.levelManager.getGridSpacing ()) * currentGrid;
				while (gridLevel[x1, (int)spawnPoint.z].isOccupiedByObject()) {
					spawnPoint = CalculateSpawnPoint (currentGrid, gridLevel);
					x1 = (int)spawnPoint.x - (this.levelManager.squareGridSize () + this.levelManager.getGridSpacing ()) * currentGrid;
				}
			}

			this.clone = Instantiate (Player, spawnPoint, Quaternion.identity) as GameObject;
			this.clone.transform.position = spawnPoint;
			iTween.ScaleFrom (this.clone, new Vector3 (0, 0, 0), 0.75f);
			this.clone.transform.parent = this.parent.transform;
			this.levelManager.PlayerIsAlive = true;
			this.levelManager.LevelState = LevelState.levelIsPlaying;
		}

		private bool playerIsBlocked (gridtile tile)
		{
			if (tile.getNorthTile () != null) {
				if (tile.getEastTile () != null) {
					if (tile.getNorthTile ().isOccupiedByObject () && tile.getEastTile ().isOccupiedByObject ()) {
						return true;
					}
				}

				if (tile.getWestTile () != null) {
					if (tile.getNorthTile ().isOccupiedByObject () && tile.getWestTile ().isOccupiedByObject ()) {
						return true;
					}
				}
			}

			if (tile.getSouthTile () != null) {
				if (tile.getEastTile () != null) {
					if (tile.getSouthTile ().isOccupiedByObject () && tile.getEastTile ().isOccupiedByObject ()) {
						return true;
					}
				}

				if (tile.getWestTile () != null) {
					if (tile.getSouthTile ().isOccupiedByObject () && tile.getWestTile ().isOccupiedByObject ()) {
						return true;
					}
				}
			}

			return false;
		}

		private void createSpawnPoints ()
		{
			List<Dictionary<string, object>> gridLevels = this.levelManager.GridLevels;
			int currentGridCount = InGamePlayerPrefs.GetInt ("currentGridCount");
			for (int j = 0; j < currentGridCount; j++) {
				Dictionary<string, object> currentGrid = gridLevels [j];
				object value = null;
				if (currentGrid.TryGetValue ("CORNERS", out value)) {
					Vector2[] corners = value as Vector2[];
					Vector3[] spawns = new Vector3[corners.Length];
					for (int i = 0; i < corners.Length; i++) {
						spawns [i] = new Vector3 (corners [i].x, 0.1f, corners [i].y);
					}
					this.respawns.Add (j, spawns);
				}
			}
		}

		private Vector3 GetSpawnPoint ()
		{
			// get a spawn point from the current grid the player is on or was on last
			Vector3[] v = this.respawns [InGamePlayerPrefs.GetInt ("currentGrid")];
			this.spawnPoint = Random.Range (0, v.Length);
			return new Vector3 (v [this.spawnPoint].x, -0.5f, v [this.spawnPoint].z);
		}
	}
}