using UnityEngine;
using System.Collections;

namespace LightsOff
{
	public class gridtile
	{
		private bool isOccupied;
		private bool hasPortal;
		private bool haslightsOn;
		private bool isCornerSpace;
		
		private gridtile north = null;
		private gridtile south = null;
		private gridtile east = null;
		private gridtile west = null;

		private GameObject grid;
		private Material lightOn;
		private Material lightOff;
		private Renderer renderer;
		private AudioSource audioSource;

		public gridtile (GameObject grid, Material lightOn, Material lightOff)
		{
			this.grid = grid;
			this.lightOn = lightOn;
			this.lightOff = lightOff;
			this.renderer = (Renderer)this.grid.GetComponent (typeof(Renderer));
			this.renderer.material = lightOff;
			this.audioSource = (AudioSource)this.grid.GetComponent (typeof(AudioSource));
		}

		public void turnLightOff ()
		{
			this.renderer.material = this.lightOff;
			this.haslightsOn = false;
			int currentLightsOn = InGamePlayerPrefs.GetInt ("currentLightsOn");
			currentLightsOn--;
			InGamePlayerPrefs.SetInt ("currentLightsOn", currentLightsOn);
			if (currentLightsOn == 0)
				EventManager.TriggerEvent ("LevelComplete");
		}

		public void turnLightOn ()
		{
			this.renderer.material = this.lightOn;
			this.haslightsOn = true;
			int currentLightsOn = InGamePlayerPrefs.GetInt ("currentLightsOn");
			currentLightsOn++;
			InGamePlayerPrefs.SetInt ("currentLightsOn", currentLightsOn);
		}

		public bool hasLightsOn ()
		{
			return this.haslightsOn;
		}

		public bool isOccupiedByObject ()
		{
			return this.isOccupied;
		}

		public void playLightOnSound ()
		{
			this.audioSource.Play ();
		}

		public Vector3 getGridTilePosition ()
		{
			return this.grid.transform.position;
		}

		public void setEastTile (gridtile tile)
		{
			this.east = tile;
		}

		public void setWestTile (gridtile tile)
		{
			this.west = tile;
		}

		public void setNorthTile (gridtile tile)
		{
			this.north = tile;
		}

		public void setSouthTile (gridtile tile)
		{
			this.south = tile;
		}

		public gridtile getNorthTile ()
		{
			return this.north;
		}

		public gridtile getSouthTile ()
		{
			return this.south;
		}

		public gridtile getEastTile ()
		{
			return this.east;
		}

		public gridtile getWestTile ()
		{
			return this.west;
		}

		public void setCornerSpace (bool b)
		{
			this.isCornerSpace = b;
		}

		public bool getIsCornerSpace ()
		{
			return this.isCornerSpace;
		}

		public void setIsOccupied (bool b)
		{
			this.isOccupied = b;
		}

		public bool HasPortal {
			get { return this.hasPortal; }
			set { this.hasPortal = value; }
		}
	}
}