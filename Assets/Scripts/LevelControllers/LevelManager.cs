using GooglePlayGames;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace LightsOff
{
	public class LevelManager : MonoBehaviour
	{
		// Grid variables
		private List<Dictionary<string, object>> gridLevels;
		private const int GRID_SPACING = 2;
		private Vector3[] gridCentres;
		private Vector2[] gridLocations;

		// Player variables
		private bool playerIsAlive;
		private bool playerHasTimeBonus;
		private PlayerState playerState;
		private Vector2 playerPosition;

		// Timer variables
		private bool gameIsPaused;

		// States
		private LevelState gameState; // state for the game
		private CreationState creationState; // state for level creation

		// Bools to support the states
		private bool gridIsBuilt;
		private bool obstaclesAreBuilt;
		private bool portalsCanSpawn;
		private bool playerCanSpawn;
		private bool levelIsComplete;
		private bool levelIsPaused;
		private bool outOfTime;

		public GameObject level;
		public GameObject GameUI;
		public InGameTimer timer;

		// Rotation tracking for the camera
		private int rotateCount;

		// Power up tracking
		private bool powerUpPickedUp;
		private string powerUpName;

		// Achievements
		private int obstaclesDestroyed;
		private readonly string OOHHHAAHHH = "CgkIl4yxreAQEAIQCA";
		private readonly string IMAWESOMEALREADY = "CgkIl4yxreAQEAIQCw";
		private readonly string INTHENICKOFTIME = "CgkIl4yxreAQEAIQDw";
		private readonly string PLAYGAMEACHIEVEMENT = "CgkIl4yxreAQEAIQAQ";

		// Audio control
		private bool audioIsMuted;

		void Start ()
		{
			this.obstaclesDestroyed = 0;
			this.rotateCount = 0;
			this.gameState = LevelState.levelIsBuilding;
			this.creationState = CreationState.gridIsBuilding;
			this.gridLevels = new List<Dictionary<string, object>> ();
			int maximumLevels = InGamePlayerPrefs.GetInt ("maximumLevels");
			this.gridCentres = new Vector3[maximumLevels];
		}

		void OnEnable ()
		{
			EventManager.StartListening ("LevelComplete", LevelComplete);
			EventManager.StartListening ("LevelPlaying", LevelPlaying);
		}

		void OnDisable ()
		{
			EventManager.StopListening ("LevelComplete", LevelComplete);
			EventManager.StopListening ("LevelPlaying", LevelPlaying);
		}

		void LevelComplete ()
		{
			this.gameState = LevelState.levelIsComplete;
		}

		void LevelPlaying ()
		{
			this.gameState = LevelState.levelIsPlaying;
			this.level.SetActive (true);
			Time.timeScale = 1.0f;
		}

		public void UnlockAchievement (string id)
		{
			if (Social.localUser.authenticated && !AchievementsTracker.Instance.AchievementIsUnlocked (id)) {
				Social.ReportProgress (id, 100.0f, (bool success) => {});
				AchievementsTracker.Instance.UpdateAchievements (id);
			}
		}
		
		void Update ()
		{
			switch (this.gameState) {
			case LevelState.levelIsBuilding:
				switch (this.creationState) {
				case CreationState.gridIsBuilding:
					if (!this.gridIsBuilt) {
						this.gridIsBuilt = true;
						this.UnlockAchievement (PLAYGAMEACHIEVEMENT);
						EventManager.TriggerEvent ("BuildGrid");
					}
					break;
				case CreationState.gridIsBuilt:
					if (!this.obstaclesAreBuilt) {
						this.obstaclesAreBuilt = true;
						EventManager.TriggerEvent ("CentreCamera");
						EventManager.TriggerEvent ("BuildObstacles");
					}
					break;
				case CreationState.obstaclesHaveSpawned:
					if (!this.portalsCanSpawn) {
						this.portalsCanSpawn = true;
						int currentGridCount = InGamePlayerPrefs.GetInt ("currentGridCount");
						if (currentGridCount > 1) {
							// we have more than one grid on the scene, portals need to be spawned
							EventManager.TriggerEvent ("BuildPortals");
						} else {
							// we can spawn the player and render the ui
							this.creationState = CreationState.portalsHaveSpawned;
						}
					}
					break;
				case CreationState.portalsHaveSpawned:
					if (!this.playerCanSpawn) {
						this.playerCanSpawn = true;
						EventManager.TriggerEvent ("SpawnPlayer");
						EventManager.TriggerEvent ("ShowUi");
						float time = Random.Range (5, 10);
						Invoke ("SpawnPowerUp", time);
					}
					break;
				}
				break;
			case LevelState.levelIsPlaying:
				if (this.levelIsComplete)
					this.levelIsComplete = false;

				if (this.levelIsPaused)
					this.levelIsPaused = false;

				if (this.outOfTime)
					this.outOfTime = false;

				if (Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown (KeyCode.Menu))
					this.gameState = LevelState.levelIsPaused;

				break;
			case LevelState.levelIsPaused:
				if (!this.levelIsPaused) {
					this.levelIsPaused = true;
					this.level.SetActive (!this.levelIsPaused);
					Time.timeScale = 0.0f;
					EventManager.TriggerEvent ("GamePaused");
				}
				break;
			case LevelState.levelIsComplete:
				if (!this.levelIsComplete) {
					this.levelIsComplete = true;
					EventManager.TriggerEvent ("CompleteUi");
					if (this.timer.getTime () <= 1)
						UnlockAchievement (INTHENICKOFTIME);
					CancelInvoke ();
					this.level.SetActive (false);
					LevelEnd ();
				}
				break;
			case LevelState.outOfTime:
				if (!outOfTime) {
					outOfTime = true;
					EventManager.TriggerEvent ("PlayerOutOfTime");
				}
				break;
			}
		}

		void SpawnPowerUp ()
		{
			EventManager.TriggerEvent ("SpawnPowerUp");
			float time = Random.Range (12, 17);
			Invoke ("SpawnPowerUp", time);
		}

		void LevelEnd ()
		{
			int currentGridCount = InGamePlayerPrefs.GetInt ("currentGridCount");			// how many grids are there
			int maxObstacles = (this.squareGridSize () - 1) * currentGridCount;				// the maximum number of obstacles in total allowed
			int currentObstaclesCount = InGamePlayerPrefs.GetInt ("currentObstacleQty");	// the number of obstacles per grid
			int obstacleCount = InGamePlayerPrefs.GetInt ("totalObstacles");				// the actual number of obstacles on all grids

			if (this.squareGridSize () >= 6 && !this.powerUpPickedUp)
				UnlockAchievement (IMAWESOMEALREADY);

			// if we can spawn more obstacles, increase the obstacle count by one and reload the game level
			if (obstacleCount < maxObstacles) {
				currentObstaclesCount++;
				InGamePlayerPrefs.SetInt ("currentObstacleQty", currentObstaclesCount);
				StartCoroutine (LoadLevel ("levelOne"));
			} else {
				int maximumLevels = InGamePlayerPrefs.GetInt ("maximumLevels");			// the maximum number of levels allowed, currently 8
				// We cant spawn more obstacles. Is the current grid size less than 8, as thats our maximum
				if (this.squareGridSize () < maximumLevels) {
					int currentLevel = InGamePlayerPrefs.GetInt ("currentLevel");
					InGamePlayerPrefs.SetInt ("currentLevel", ++currentLevel);
					StartCoroutine (LoadLevel ("levelLoader"));
				} else {
					// our grid(s) is 8 x 8. Are there less than 8 grids?
					if (currentGridCount < maximumLevels) {
						// yes there are.
						currentGridCount++;
						InGamePlayerPrefs.SetInt ("currentGridCount", currentGridCount);
						InGamePlayerPrefs.SetInt ("currentLevel", 0);
						StartCoroutine (LoadLevel ("levelLoader"));
					} else {
						// our grid is 8 x 8 and there are 8 grids. Game end
						this.level.SetActive (false);
						this.gameState = LevelState.gameFinished;
						EventManager.TriggerEvent ("GameOverUi");
					}
				}
			}
		}

		IEnumerator LoadLevel (string level)
		{
			InGamePlayerPrefs.SetInt ("currentGrid", 0);
			yield return new WaitForSeconds (5f);
			Application.LoadLevel (level);
		}

		public CreationState CreationState {
			get { return this.creationState; }
			set { this.creationState = value; }
		}

		public List<Dictionary<string, object>> GridLevels {
			get { return this.gridLevels; }
			set { this.gridLevels = value; }
		}

		public Vector3[] GridCentres {
			get { return this.gridCentres; }
			set { this.gridCentres = value; }
		}

		public Vector2[] GridLocations {
			get { return this.gridLocations; }
			set { this.gridLocations = value; }
		}

		public LevelState LevelState {
			get { return this.gameState; }
			set {
				if (value == LevelState.levelIsBuilding) {
					this.gridIsBuilt = false;
					this.obstaclesAreBuilt = false;
					this.portalsCanSpawn = false;
					this.playerCanSpawn = false;
					this.creationState = CreationState.gridIsBuilding;
					this.playerState = PlayerState.standard;
				}
				this.gameState = value;
			}
		}

		public bool PlayerIsAlive {
			get { return this.playerIsAlive; }
			set { this.playerIsAlive = value; }
		}

		public bool PlayerHasTimeBonus {
			get { return this.playerHasTimeBonus; }
			set { this.playerHasTimeBonus = value; }
		}

		public bool GameIsPaused {
			get { return this.gameIsPaused; }
			set { this.gameIsPaused = value; }
		}

		public int getGridSpacing ()
		{
			return GRID_SPACING;
		}

		public PlayerState PlayerState {
			get { return this.playerState; }
			set { this.playerState = value; }
		}

		public int RotateCount {
			get { return this.rotateCount; }
			set { this.rotateCount = value; }
		}

		public int squareGridSize ()
		{
			int currentLevel = InGamePlayerPrefs.GetInt ("currentLevel");
			return currentLevel + 3;
		}

		public Vector2 PlayerPosition {
			get { return this.playerPosition; }
			set { this.playerPosition = value; }
		}

		public int ObstaclesDestroyed {
			get { return this.obstaclesDestroyed; }
			set {
				this.obstaclesDestroyed = value;
				if (this.obstaclesDestroyed == 3) {
					UnlockAchievement (OOHHHAAHHH);
				}
			}
		}

		public bool PowerUpPickedup {
			get { return this.powerUpPickedUp; }
			set { this.powerUpPickedUp = value; }
		}

		public string PowerUpName {
			get { return this.powerUpName; }
			set {
				this.powerUpName = value;
				EventManager.TriggerEvent ("PowerUpNameChange");
			}
		}

		public bool AudioIsMuted {
			get { return this.audioIsMuted; }
			set {
				this.audioIsMuted = value;
				AudioListener.pause = this.audioIsMuted;
			}
		}
	}
}