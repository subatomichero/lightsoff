﻿using UnityEngine;
using System.Collections;

namespace LightsOff
{
	public class InGameTimer : MonoBehaviour
	{
		public LevelManager levelManager;
		public float timeElapsed;
		private float prevTime;
		private bool canStartCountDown;

		void Start ()
		{
			this.timeElapsed = 60f;
			int currentGridCount = InGamePlayerPrefs.GetInt ("currentGridCount");
			if (currentGridCount > 1) {
				this.timeElapsed = 30f * currentGridCount;
				if (this.timeElapsed < 60f)
					this.timeElapsed = 60f;
			}
			this.prevTime = this.timeElapsed;
		}

		void OnEnable ()
		{
			EventManager.StartListening ("StartCountdown", StartCountdown);
			EventManager.StartListening ("UpdateTime", UpdateTime);
		}

		void OnDisable ()
		{
			EventManager.StopListening ("StartCountdown", StartCountdown);
			EventManager.StopListening ("UpdateTime", UpdateTime);
		}

		void StartCountdown ()
		{
			this.canStartCountDown = true;
		}

		void UpdateTime ()
		{
			this.prevTime = this.prevTime / 2;
			this.timeElapsed = this.prevTime;
		}

		void Update ()
		{
			if (this.canStartCountDown) {
				if (this.levelManager.PlayerState != PlayerState.hasTime && this.levelManager.LevelState != LevelState.levelIsComplete
					&& InGamePlayerPrefs.GetInt ("currentLives") >= 0 && !this.levelManager.GameIsPaused) {
					if (this.timeElapsed > 0) {
						this.timeElapsed -= Time.deltaTime;
					} else {
						this.levelManager.LevelState = LevelState.outOfTime;
					}
				}
			}
		}
		
		public int getTime ()
		{
			return Mathf.RoundToInt (this.timeElapsed);
		}
	}
}