using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace LightsOff
{
	public class gridBuilder : MonoBehaviour
	{
		private Vector2[] corners;
		private string[] sparkPositions;
		private float cumTimer = 0f;
		private int gridSize;
		private readonly string HOWMANYGRIDS = "CgkIl4yxreAQEAIQBg";

		public Material lightOn;
		public Material lightOff;
		public LevelManager levelManager;
		public GameObject parent;
		
		void Awake ()
		{
			this.gameObject.SetActive (true);
		}

		void Start ()
		{
			int maximumLevels = InGamePlayerPrefs.GetInt ("maximumLevels");
			int gridSpacing = this.levelManager.getGridSpacing ();
			this.gridSize = this.levelManager.squareGridSize ();
			Vector2[] gridLocations = new Vector2[maximumLevels];
			// manual placement of each grid, this needs to be clever
			// each grid needs to have space from the next / previous grid
			gridLocations [0] = new Vector2 (0, 0);
			for (int i = 1; i < gridLocations.Length; i++) {
				// because the Z will always be 0, lets get the x of the previous grid
				int x = Mathf.RoundToInt (gridLocations [i - 1].x);
				gridLocations [i] = new Vector2 ((x + this.gridSize) + gridSpacing, 0);
			}
			this.levelManager.GridLocations = gridLocations;
			this.sparkPositions = new string[4];
			this.sparkPositions [0] = " Top";
			this.sparkPositions [1] = " Bottom";
			this.sparkPositions [2] = " Right";
			this.sparkPositions [3] = " Left";
		}

		void OnEnable ()
		{
			EventManager.StartListening ("BuildGrid", StartBuilding);
		}

		void OnDisable ()
		{
			EventManager.StopListening ("BuildGrid", StartBuilding);
		}

		void StartBuilding ()
		{
			StartCoroutine (createSquareGridTimed ());
		}
		
		private IEnumerator createSquareGridTimed ()
		{
			// The values used in the loops will change depending on the level, so the grid is different
			int lightsOnCount = 0;
			int currentGridCount = InGamePlayerPrefs.GetInt ("currentGridCount");
			if (currentGridCount > 1)
				this.levelManager.UnlockAchievement (HOWMANYGRIDS);
			Vector2[] gridLocations = this.levelManager.GridLocations;
			List<Dictionary<string, object>> gridLevels = new List<Dictionary<string, object>> ();

			// For every grid
			for (int i = 0; i < currentGridCount; i++) {
				int gridColumn = 0;
				this.corners = new Vector2[4];
				short startCol = (short)gridLocations [i].x;
				short startRow = (short)gridLocations [i].y;
				int endCol = startCol + this.gridSize;
				int endRow = startRow + this.gridSize;
				gridtile[,] gridLevel = new gridtile[this.gridSize, this.gridSize];

				// Loop through the columns
				for (short col = startCol; col < endCol; col++) {
					int gridRow = 0;

					// Loop through the rows
					for (short row = startRow; row < endRow; row++) {
						// Add each piece of the grid to the screen and animate it
						GameObject g = ObjectPooler.GetPooledGrid ();
						g.transform.position = new Vector3 (col, 15, row);
						g.SetActive (true);
						g.transform.parent = this.parent.transform;
						iTween.MoveTo (g, new Vector3 (col, -1, row), 0.7f);
						// Decide what grid pieces will shoot off sparks when they are in position
						this.shootOffSparks (g, gridColumn, gridRow);
						lightsOnCount++;
						gridLevel [gridColumn, gridRow] = new gridtile (g, lightOn, lightOff);
						yield return new WaitForSeconds (0.10f + this.cumTimer);
						this.cumTimer -= 0.005f;
						gridRow++;
					}
					gridColumn++;
				}

				// put the current grid centre into an array we move the camera to and from
				this.setupGrid (gridLevel);
				this.setupGridCentre (gridLevel, i);
				
				// create a map containing all the level details we may need later
				Dictionary<string, object> levelDetails = new Dictionary<string, object> ();
				levelDetails.Add ("LEVEL", i);
				levelDetails.Add ("GRID", gridLevel);
				levelDetails.Add ("CORNERS", this.corners);
				gridLevels.Add (levelDetails);
				yield return new WaitForSeconds (0.5f);
			}
			this.levelManager.GridLevels = gridLevels;
			StartCoroutine (this.toggleLightingTimed ());
		}
		
		IEnumerator toggleLightingTimed ()
		{
			this.cumTimer = 0f;
			List<Dictionary<string, object>> gridLevels = this.levelManager.GridLevels;
			foreach (Dictionary<string, object> map in gridLevels) {
				object obj = null;
				if (map.TryGetValue ("GRID", out obj)) {
					gridtile[,] gridLevel = obj as gridtile[,];
					for (int col = 0; col < this.gridSize; col++) {
						for (int row = 0; row < this.gridSize; row++) {
							gridLevel [col, row].turnLightOn ();
							gridLevel [col, row].playLightOnSound ();
							yield return new WaitForSeconds (0.05f + this.cumTimer);
							this.cumTimer -= 0.005f;
						}
					}
				}
			}
			this.levelManager.CreationState = CreationState.gridIsBuilt;
			this.gameObject.SetActive (false);
		}
		
		private void shootOffSparks (GameObject grid, int col, int row)
		{
			// We are not sparking at the first corner segment
			if (col == 0 && row <= this.gridSize) {
				// theres a 50% chance sparks on the bottom, right and top will spark, unless col is at the edge
				this.sparkPlayer (grid, " Left");
			} else if (col >= 0 && row == this.gridSize) {
				// we dont want any sparks shooting at the top
				this.sparkPlayer (grid, " Top");
			} else if (col > 0 && col < this.gridSize && row == 0) {
				// we dont want any sparks shooting at the bottom
				this.sparkPlayer (grid, " Bottom");
			} else if (col == this.gridSize
				&& row >= 0 && row <= this.gridSize) {
				this.sparkPlayer (grid, " Right");	
			} else {
				// we can play any spark
				this.sparkPlayer (grid, null);
			}
		}
		
		private void sparkPlayer (GameObject grid, string s)
		{
			int random = Random.Range (0, 3);
			// theres a 50% chance sparks will play
			if (random != 1) {
				Transform spark = grid.transform.FindChild ("GridSpark" + this.getSparkToPlay (s));
				// play the particle effect and sound fx
				spark.GetComponent<ParticleSystem> ().Play ();
				spark.GetComponent<AudioSource> ().Play ();
			}
		}
		
		private string getSparkToPlay (string badSpark)
		{
			// return one of the strings in the sparks to play list that doesnt equal bad spark
			if (badSpark == null)
				return this.getSparkToPlay ();
			string ret = this.sparkPositions [Random.Range (0, this.sparkPositions.Length)];
			while (ret.Equals(badSpark))
				ret = this.sparkPositions [Random.Range (0, this.sparkPositions.Length)];
			return ret;
		}
		
		private string getSparkToPlay ()
		{
			return this.sparkPositions [Random.Range (0, this.sparkPositions.Length)];	
		}
		
		private void setupGrid (gridtile[,] gridLevel)
		{
			// this function sets up where north, south are etc
			int cornerCounter = 0;
			for (int row = 0; row < this.gridSize; row++) {
				for (int col = 0; col < this.gridSize; col++) {
					if (row + 1 < this.gridSize)
						gridLevel [row, col].setEastTile (gridLevel [row + 1, col]);
					if (row - 1 >= 0)
						gridLevel [row, col].setWestTile (gridLevel [row - 1, col]);
					if (col + 1 < this.gridSize)
						gridLevel [row, col].setNorthTile (gridLevel [row, col + 1]);
					if (col - 1 >= 0)
						gridLevel [row, col].setSouthTile (gridLevel [row, col - 1]);
					if (this.isCornerGrid (row, col, this.gridSize)) {
						gridLevel [row, col].setCornerSpace (true);
						this.corners [cornerCounter++] = new Vector2 (row, col);
					}
				}
			}
		}
		
		private void setupGridCentre (gridtile[,] level, int counter)
		{
			int size = this.gridSize * this.gridSize;
			Vector3[] positions = new Vector3[size];
			int index = 0;

			// collapse the 2d array into a 1d array
			for (int row = 0; row < this.gridSize; row++) {
				for (int col = 0; col < this.gridSize; col++) {
					positions [index++] = new Vector3 (row, 1, col);
				}
			}

			// we have all the grid positions in an array
			Vector3 position;
			int count;
			if ((positions.Length & 1) == 0) { // even
				// if this is 4 x 4
				// we work this out differently
				int row = Mathf.RoundToInt (this.gridSize * 0.5f);
				count = 1;
				for (int i = 1; i < row; i++) {
					count += this.gridSize;
				}
				position = positions [count];
				if (counter > 0) {
					position.x += (this.levelManager.squareGridSize () + this.levelManager.getGridSpacing () * counter);
				}
				this.levelManager.GridCentres [counter] = new Vector3 (position.x + 0.5f, 1, position.z + 0.25f);
			} else { // odd
				// e.g. if this is 5 x 5
				count = Mathf.RoundToInt ((positions.Length - 1) * 0.5f);
				// should be an even number for 5 x 5 should be 12 (25 - 1 = 24 / 2 = 12)
				// for 3 x 3 should be (9 - 1 = 8 / 2 = 4), get the 4th element from the positions array
				position = positions [count];
				if (counter > 0) {
					position.x += (this.levelManager.squareGridSize () + this.levelManager.getGridSpacing () * counter);
				}
				this.levelManager.GridCentres [counter] = new Vector3 (position.x, 1, position.z);
			}
		}
		
		private bool isCornerGrid (int row, int col, int size)
		{
			return (row == 0 && col == 0 || row == 0 && col == (size - 1) ||
				row == (size - 1) && col == 0 || row == (size - 1) && col == size - 1);
		}
	}
}