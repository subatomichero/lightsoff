﻿namespace LightsOff
{
	public enum CreationState
	{
		gridIsBuilding,
		gridIsBuilt,
		obstaclesHaveSpawned,
		portalsHaveSpawned
	}
}