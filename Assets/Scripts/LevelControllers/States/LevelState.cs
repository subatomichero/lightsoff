﻿namespace LightsOff
{
	public enum LevelState
	{
		levelIsBuilding,
		levelIsComplete,
		levelIsPlaying,
		levelIsPaused,
		outOfTime,
		gameFinished
	}
}