﻿namespace LightsOff
{
	public enum PlayerState
	{
		standard,
		hasSpeed,
		hasScore,
		hasTime,
		hasDemo,
		hasNoEnergy
	}
}