using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace LightsOff
{
	public class ObstacleBuilder : MonoBehaviour
	{
		private GameObject obstacle;
		public LevelManager levelManager;
		public GameObject parent;

		void Awake ()
		{
			this.gameObject.SetActive (true);
		}

		void OnEnable ()
		{
			EventManager.StartListening ("BuildObstacles", StartPlacingObstacles);
		}

		void OnDisable ()
		{
			EventManager.StopListening ("BuildObstacles", StartPlacingObstacles);
		}

		void StartPlacingObstacles ()
		{
			StartCoroutine (buildObstacles ());
		}

		IEnumerator buildObstacles ()
		{
			int gridSize = this.levelManager.squareGridSize ();
			int obstacleQty = InGamePlayerPrefs.GetInt ("currentObstacleQty"); // per grid
			int currentGridCount = InGamePlayerPrefs.GetInt ("currentGridCount");
			int obstacleCount = 0;
			for (int count = 0; count < currentGridCount; count++) {
				gridtile[,] gridLevel = this.levelManager.GridLevels [count] ["GRID"] as gridtile[,];
				float x = 0f;
				Vector2 position;
				for (int i = 0; i < obstacleQty; i++) {
					obstacleCount++;
					if (obstacleCount > 1) {
						position = this.generateRandomCoord (gridLevel);
						x = position.x;
						if (count > 0) {
							x -= (gridSize + this.levelManager.getGridSpacing ()) * count;
							x = Mathf.Abs (x);
						}
						while (gridLevel[(int) x, (int)position.y].isOccupiedByObject() || this.hasCornerBlock(gridLevel, position, x)) {
							position = this.generateRandomCoord (gridLevel);
							x = position.x;
							if (count > 0) {
								x -= (gridSize + this.levelManager.getGridSpacing ()) * count;
								x = Mathf.Abs (x);
							}
						}
					} else {
						// we must have only one obstacle, lets put it in a corner
						Vector2[] currentCorners = this.levelManager.GridLevels [count] ["CORNERS"] as Vector2[];
						int random = Random.Range (0, currentCorners.Length); // get a corner
						x = currentCorners [random].x;
						position.x = x + gridLevel [0, 0].getGridTilePosition ().x;
						position.y = currentCorners [random].y;
					}
					this.obstacle = ObjectPooler.GetPooledObstacle ();
					this.obstacle.transform.position = new Vector3 (position.x, 15, position.y);
					this.obstacle.SetActive (true);
					this.obstacle.transform.parent = this.parent.transform;
					iTween.MoveTo (this.obstacle, new Vector3 (position.x, -0.55f, position.y), 0.5f);
					gridLevel [(int)x, (int)position.y].setIsOccupied (true);
					yield return new WaitForSeconds (1);
					gridLevel [(int)x, (int)position.y].turnLightOff ();
				}
				this.levelManager.GridLevels [count] ["GRID"] = gridLevel;
			}
			InGamePlayerPrefs.SetInt ("totalObstacles", obstacleCount);
			this.levelManager.CreationState = CreationState.obstaclesHaveSpawned;
			this.gameObject.SetActive (false);
		}

		private Vector2 generateRandomCoord (gridtile[,] gridLevel)
		{
			Vector2 position = new Vector2 (0, 0);
			int x = Random.Range (0, this.levelManager.squareGridSize ());
			int z = Random.Range (0, this.levelManager.squareGridSize ());
			Vector3 gridPos = gridLevel [x, z].getGridTilePosition ();
			position = new Vector2 (gridPos.x, gridPos.z);
			return position;
		}

		private bool hasCornerBlock (gridtile[,] gridLevel, Vector2 position, float x)
		{
			// get each grid tile
			gridtile northTile = gridLevel [(int)x, (int)position.y].getNorthTile ();
			if (northTile == null)
				return false;
			gridtile westTile = gridLevel [(int)x, (int)position.y].getWestTile ();
			if (westTile == null)
				return false;
			gridtile eastTile = gridLevel [(int)x, (int)position.y].getEastTile ();
			if (eastTile == null)
				return false;
			gridtile southTile = gridLevel [(int)x, (int)position.y].getSouthTile ();
			if (southTile == null)
				return false;

			// Check if the corners are blocked
			if (northTile.getEastTile () != null && northTile.getIsCornerSpace () && northTile.getEastTile ().isOccupiedByObject ()) {
				return true;
			} else if (northTile.getWestTile () != null && northTile.getIsCornerSpace () && northTile.getWestTile ().isOccupiedByObject ()) {
				return true;
			} else if (westTile.getSouthTile () != null && westTile.getIsCornerSpace () && westTile.getSouthTile ().isOccupiedByObject ()) {
				return true;
			} else if (eastTile.getSouthTile () != null && eastTile.getIsCornerSpace () && eastTile.getSouthTile ().isOccupiedByObject ()) {
				return true;
			} else if (southTile.getEastTile () != null && southTile.getIsCornerSpace () && southTile.getEastTile ().isOccupiedByObject ()) {
				return true;
			} else if (southTile.getWestTile () != null && southTile.getIsCornerSpace () && southTile.getWestTile ().isOccupiedByObject ()) {
				return true;
			} else if (westTile.getNorthTile () != null && westTile.getIsCornerSpace () && westTile.getNorthTile ().isOccupiedByObject ()) {
				return true;
			} else if (eastTile.getNorthTile () != null && eastTile.getIsCornerSpace () && eastTile.getNorthTile ().isOccupiedByObject ()) {
				return true;
			}
			return false;
		}
	}
}