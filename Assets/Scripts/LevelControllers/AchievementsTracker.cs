﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace LightsOff
{
	public class AchievementsTracker
	{
		private static AchievementsTracker sInstance = new AchievementsTracker ();
		private Dictionary<string,bool> UnLockedAchievements = new Dictionary<string,bool> ();

		public static AchievementsTracker Instance {
			get { return sInstance; }
		}

		public bool AchievementIsUnlocked (string id)
		{
			return UnLockedAchievements.ContainsKey (id);
		}

		public void UpdateAchievements (string id)
		{
			UnLockedAchievements [id] = true;
		}

		public override string ToString ()
		{
			string s = "";
			UnLockedAchievements ["CgkIl4yxreAQEAIQBg"] = true;
			foreach (KeyValuePair<string, bool> entry in UnLockedAchievements) {
				s += entry.Key + "-" + entry.Value + ",";
			}
			return string.Format ("AchievementsTracker:{0}", s);
		}

		private AchievementsTracker FromString (string s)
		{
			AchievementsTracker t = new AchievementsTracker ();
			string[] p = s.Split (new char[] { ':' });
			if (!p [0].StartsWith ("AchievementsTracker")) {
				return t;
			}

			string[] a = p [1].Split (new char[] { ',' });
			for (int i = 0; i < a.Length; i++) {
				string[] entry = a [i].Split (new char[] { '-' });
				string key = entry [0];
				bool value = Convert.ToBoolean (entry [1]);
				t.UnLockedAchievements [key] = value;
			}
			return t;
		}
	}
}