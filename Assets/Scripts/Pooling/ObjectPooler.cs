﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace LightsOff
{
	public class ObjectPooler : MonoBehaviour
	{
		private static ObjectPooler current;
		public bool willGrow = true;
		public GameObject pooledGrid;
		public GameObject[] pooledPowerUps;
		public GameObject pooledPortal;
		public GameObject pooledObstacle;
		public GameObject pooledExplosion;

		List<List<GameObject>> pooledObjects;
		
		public static ObjectPooler instance {
			get {
				if (!current) {
					current = FindObjectOfType (typeof(ObjectPooler)) as ObjectPooler;
					if (!current) {
						Debug.LogError ("There needs to be one active Object Pooler");
					}
				}
				return current;
			}
		}

		void Awake ()
		{
			current = this;
			current.RePool ();
		}

		void PoolGrids (int currentGridCount)
		{
			List<GameObject> list = new List<GameObject> ();
			int currentLevel = InGamePlayerPrefs.GetInt ("currentLevel");
			int pooledGridSize = ((currentLevel + 3) * (currentLevel + 3)) * currentGridCount;
			for (int i = 0; i < pooledGridSize; i++) {
				GameObject obj = (GameObject)Instantiate (pooledGrid);
				obj.SetActive (false);
				list.Add (obj);
			}
			pooledObjects.Insert (0, list);
		}

		void PoolPowerups ()
		{
			List<GameObject> list = new List<GameObject> ();
			for (int i = 0; i < pooledPowerUps.Length; i++) {
				GameObject obj = (GameObject)Instantiate (pooledPowerUps [i]);
				obj.SetActive (false);
				list.Add (obj);
			}
			pooledObjects.Insert (1, list);
		}

		void PoolObstacles (int currentGridCount)
		{
			int currentObstacleCount = InGamePlayerPrefs.GetInt ("currentObstacleQty");
			int pooledObstacleSize = currentObstacleCount * currentGridCount;
			List<GameObject> list = new List<GameObject> ();
			for (int i = 0; i < pooledObstacleSize; i++) {
				GameObject obj = (GameObject)Instantiate (pooledObstacle);
				obj.SetActive (false);
				list.Add (obj);
			}
			pooledObjects.Insert (2, list);
		}

		void PoolPortals ()
		{
			int pooledPortalSize = 14;
			List<GameObject> list = new List<GameObject> ();
			for (int i = 0; i < pooledPortalSize; i++) {
				GameObject obj = (GameObject)Instantiate (pooledPortal);
				obj.SetActive (false);
				list.Add (obj);
			}
			pooledObjects.Insert (4, list);
		}

		void PoolExplosions ()
		{
			List<GameObject> list = new List<GameObject> ();
			for (int i = 0; i < 8; i++) {
				GameObject obj = (GameObject)Instantiate (pooledExplosion);
				obj.SetActive (false);
				list.Add (obj);
			}
			pooledObjects.Insert (3, list);
		}

		void RePool ()
		{
			int currentGridCount = InGamePlayerPrefs.GetInt ("currentGridCount");
			pooledObjects = new List<List<GameObject>> ();
			PoolGrids (currentGridCount); // 0
			PoolPowerups (); // 1
			PoolObstacles (currentGridCount); // 2
			PoolExplosions (); // 3
			if (currentGridCount > 1)
				PoolPortals (); // 3
		}

		public static GameObject GetPooledGrid ()
		{
			return current.GetPooledObject (0);
		}

		public static GameObject GetPooledPowerUp (int r)
		{
			return current.pooledObjects [1] [r];
		}

		public static GameObject GetPooledObstacle ()
		{
			return current.GetPooledObject (2);
		}

		public static GameObject GetPooledExplosion ()
		{
			return current.GetPooledObject (3);
		}

		public static GameObject GetPooledPortal ()
		{
			return current.GetPooledObject (4);
		}

		private GameObject GetPooledObject (int index)
		{
			for (int i = 0; i < pooledObjects[index].Count; i++) {
				if (!pooledObjects [index] [i].activeInHierarchy) {
					return pooledObjects [index] [i];
				}
			}

			if (willGrow) {
				GameObject obj = (GameObject)Instantiate (pooledObjects [index] [0]);
				pooledObjects [index].Add (obj);
				return obj;
			}
			return null;
		}
	}
}