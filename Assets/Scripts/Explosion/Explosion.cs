﻿using UnityEngine;
using System.Collections;

namespace LightsOff
{
	public class Explosion : MonoBehaviour
	{
		void OnEnable ()
		{
			Invoke ("Destroy", 2f);
		}

		void Destroy ()
		{
			gameObject.SetActive (false);
		}
	}
}