using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace LightsOff
{
	// increase current grid count for testing
	public static class InGamePlayerPrefs
	{
		private static Dictionary<string, object> map = new Dictionary<string, object> ();

		private static void _addPair<T> (string key, ref T value)
		{
			map [key] = value;
		}

		private static object _getValue<T> (string key)
		{
			object value;
			if (map.TryGetValue (key, out value)) {
				return value;
			}
			return null;
		}

		public static void SetInt (string key, int value)
		{
			_addPair (key, ref value);
		}

		public static int GetInt (string key)
		{
			return Convert.ToInt32 (_getValue<int> (key));
		}

		public static void SetFloat (string key, float value)
		{
			_addPair (key, ref value);
		}

		public static float GetFloat (string key)
		{
			return Convert.ToSingle (_getValue<float> (key));
		}

		public static void SetString (string key, string value)
		{
			_addPair (key, ref value);
		}

		public static string GetString (string key)
		{

			return Convert.ToString (_getValue<string> (key));
		}

		public static void init ()
		{
			if (Time.timeScale != 1)
				Time.timeScale = 1;
			SetInt ("currentGrid", 0);
			SetInt ("currentLevel", 0);
			SetInt ("currentLives", 3);
			SetInt ("currentScore", 0);
			SetInt ("currentGridCount", 1);
			SetInt ("currentLightsOn", 0);
			SetInt ("currentObstacleQty", 1);
			SetInt ("maximumLevels", 8);
			SetInt ("canTeleport", 1);
		}
	}
}