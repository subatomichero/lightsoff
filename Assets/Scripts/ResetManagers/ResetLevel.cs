using UnityEngine;
using System.Collections;

namespace LightsOff
{
	public class ResetLevel : MonoBehaviour
	{
		IEnumerator Start ()
		{
			yield return new WaitForSeconds (2);
			AdBuddizBinding.ShowAd ();
			InGamePlayerPrefs.SetInt ("currentObstacleQty", 1);
			Application.LoadLevel ("levelOne");
		}
	}
}