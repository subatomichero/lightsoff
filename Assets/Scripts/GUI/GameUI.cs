﻿using GooglePlayGames;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
using System.Collections;

namespace LightsOff
{
	public class GameUI : MonoBehaviour
	{
		public Text Lights;
		public Text Score;
		public Text Time;
		public Text Lives;
		public Text FinalScore;
		public Text Message;
		public Text PowerUpName;
		public Text MuteAudio;

		public InGameTimer timer;
		public Canvas GameUi;
		public Canvas QuitMenu;
		public Canvas PauseMenu;
		public Canvas CompleteUi;
		public Canvas GameOver;
		public LevelManager levelManager;

		private readonly string FAILTASTIC = "CgkIl4yxreAQEAIQBw";
		private readonly string LEADERBOARD = "CgkIl4yxreAQEAIQAA";
		private readonly string AMAZEBALLS = "CgkIl4yxreAQEAIQCQ";
		private readonly string TWOTHOUSAND = "CgkIl4yxreAQEAIQDA";
		private readonly string FIVETHOUSAND = "CgkIl4yxreAQEAIQDQ";
		private readonly string TENTHOUSAND = "CgkIl4yxreAQEAIQDg";

		void Start ()
		{
			this.Message.text = "Game Over";
		}

		void OnEnable ()
		{
			EventManager.StartListening ("ScoreUpdate", UpdateUi);
			EventManager.StartListening ("GamePaused", ShowPauseMenu);
			EventManager.StartListening ("ShowUi", EnableGameUi);
			EventManager.StartListening ("CompleteUi", EnableCompleteUi);
			EventManager.StartListening ("GameOverUi", EnableGameOverUi);
			EventManager.StartListening ("PowerUpNameChange", PowerUpNameChange);
		}

		void OnDisable ()
		{
			EventManager.StopListening ("ScoreUpdate", UpdateUi);
			EventManager.StopListening ("GamePaused", ShowPauseMenu);
			EventManager.StopListening ("ShowUi", EnableGameUi);
			EventManager.StopListening ("CompleteUi", EnableCompleteUi);
			EventManager.StopListening ("GameOverUi", EnableGameOverUi);
			EventManager.StopListening ("PowerUpNameChange", PowerUpNameChange);
		}

		void PowerUpNameChange ()
		{
			this.PowerUpName.text = this.levelManager.PowerUpName;
			bool enabled = (this.levelManager.PowerUpName != "" ? true : false);
			this.PowerUpName.gameObject.SetActive (enabled);
		}

		void EnableGameOverUi ()
		{
			this.CompleteUi.gameObject.SetActive (false);
			this.GameUi.gameObject.SetActive (false);
			this.GameOver.gameObject.SetActive (true);
			int currentScore = InGamePlayerPrefs.GetInt ("currentScore");
			if (this.levelManager.LevelState == LevelState.gameFinished) {
				this.Message.text = "Game Complete!";
				this.levelManager.UnlockAchievement (AMAZEBALLS);
			}
			this.FinalScore.text = "Final Score: " + currentScore;
			if (currentScore == 0)
				this.levelManager.UnlockAchievement (FAILTASTIC);
			Social.ReportScore (currentScore, LEADERBOARD, (bool success) => {
				StartCoroutine (EndGame ());
			});
		}

		IEnumerator EndGame ()
		{
			yield return new WaitForSeconds (2f);
			Application.LoadLevel ("credits");
		}

		void EnableGameUi ()
		{
			this.GameUi.gameObject.SetActive (true);
			this.UpdateUi ();
		}

		void EnableCompleteUi ()
		{
			this.GameUi.gameObject.SetActive (false);
			this.CompleteUi.gameObject.SetActive (true);
		}

		void UpdateUi ()
		{
			int score = InGamePlayerPrefs.GetInt ("currentScore");
			switch (score) {
			case 2000:
				this.levelManager.UnlockAchievement (TWOTHOUSAND);
				break;
			case 5000:
				this.levelManager.UnlockAchievement (FIVETHOUSAND);
				break;
			case 10000:
				this.levelManager.UnlockAchievement (TENTHOUSAND);
				break;
			}
			Lights.text = "Lights: " + InGamePlayerPrefs.GetInt ("currentLightsOn");
			Score.text = "Score: " + score;
			Lives.text = "Lives: " + InGamePlayerPrefs.GetInt ("currentLives");
		}

		void Update ()
		{
			Time.text = "Time: " + timer.getTime ();
			switch (this.levelManager.PlayerState) {
			case PlayerState.hasScore:
				this.Score.color = Color.green;
				break;
			case PlayerState.hasTime:
				this.Time.color = Color.green;
				break;
			default:
				this.Score.color = Color.white;
				this.Time.color = Color.white;
				break;
			}
		}

		public void MovePlayerUp ()
		{
			EventManager.TriggerEvent ("PlayerUp");
		}

		public void MovePlayerDown ()
		{
			EventManager.TriggerEvent ("PlayerDown");
		}

		public void MovePlayerLeft ()
		{
			EventManager.TriggerEvent ("PlayerLeft");
		}

		public void MovePlayerRight ()
		{
			EventManager.TriggerEvent ("PlayerRight");
		}

		public void RotateCameraLeft ()
		{
			EventManager.TriggerEvent ("RotateLeft");
		}

		public void RotateCameraRight ()
		{
			EventManager.TriggerEvent ("RotateRight");
		}

		public void ShowPauseMenu ()
		{
			this.GameUi.gameObject.SetActive (false);
			this.PauseMenu.gameObject.SetActive (true);
		}

		public void QuitGame ()
		{
			this.PauseMenu.gameObject.SetActive (false);
			this.QuitMenu.gameObject.SetActive (true);
		}
		
		public void ResumeGame ()
		{
			this.PauseMenu.gameObject.SetActive (false);
			this.GameUi.gameObject.SetActive (true);
			EventManager.TriggerEvent ("LevelPlaying");
		}
		
		public void YesPressed ()
		{
			InGamePlayerPrefs.init ();
			Application.LoadLevel ("mainMenu");
		}
		
		public void NoPressed ()
		{
			this.QuitMenu.gameObject.SetActive (false);
			this.PauseMenu.gameObject.SetActive (true);
		}

		public void ControlAudio ()
		{
			this.levelManager.AudioIsMuted = !this.levelManager.AudioIsMuted;
			if (this.levelManager.AudioIsMuted) {
				this.MuteAudio.text = "Unmute Audio";
			} else {
				this.MuteAudio.text = "Mute Audio";
			}
		}
	}
}