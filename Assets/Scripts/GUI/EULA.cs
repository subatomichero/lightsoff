﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace LightsOff
{
	class EULA : MonoBehaviour
	{
		public void AcceptPressed ()
		{
			PlayerPrefs.SetInt ("EULA", 1);
			PlayerPrefs.Save ();
			StartCoroutine (StartGame ());
		}

		IEnumerator StartGame ()
		{
			yield return new WaitForSeconds (1f);
			Application.LoadLevel ("mainMenu");
		}

		public void DeclinePressed ()
		{
			Application.Quit ();
		}
	}
}