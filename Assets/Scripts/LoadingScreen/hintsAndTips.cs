using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace LightsOff
{
	public class hintsAndTips : MonoBehaviour
	{
		void Start ()
		{
			string[] tips = new string[] {
				"Be careful, Some power-ups can cause you trouble!",
				"Grabbing power-ups can have adverse effects. Beware!",
				"The pause menu can be accessed by pressing the menu button.",
				"The demolition bonus allows you to pulverize any obstacles in your path!",
				"The score bonus doubles your score generation and removes your light penalty!",
				"The time bonus freezes time for a short duration. Be quick!",
				"The speed bonus helps you along your way. Watch out for the void!",
				"Falling off the grid will cost you one life. Plan your trip carefully!"
			};
			int i = Random.Range (0, tips.Length);
			GetComponent<Text> ().text = tips [i];

			System.GC.Collect ();
		}
	}
}