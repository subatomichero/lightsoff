using UnityEngine;
using System.Collections;

namespace LightsOff
{
	public class PowerUp : MonoBehaviour
	{
		public AudioClip pickupSound;
		private Hashtable idle, collectedSpin, collectedMove, collectedShrink, missedSpin;
		private AudioSource audioSource;
		private SphereCollider sphereCollider;

		void Start ()
		{
			iTween.Init (gameObject);
			this.audioSource = (AudioSource)this.GetComponent (typeof(AudioSource));
			this.initCollectedHashTables ();
			this.initMissedHashTables ();
			iTween.RotateAdd (gameObject, idle);
		}

		void OnEnable ()
		{
			StartCoroutine (missedPU ());
		}

		void OnDisable ()
		{
			CancelInvoke ();
			StopAllCoroutines ();
		}
		
		private void initCollectedHashTables ()
		{
			// initalise the idle hashtable
			this.idle = new Hashtable ();
			this.idle.Add ("name", "idleAnim");
			this.idle.Add ("amount", new Vector3 (0f, 360f, 0f));
			this.idle.Add ("time", 4f);
			this.idle.Add ("delay", 0f);
			this.idle.Add ("easetype", iTween.EaseType.easeOutSine);
			this.idle.Add ("looptype", iTween.LoopType.loop);
			
			// initialise the collectedspin hashtable
			this.collectedSpin = new Hashtable ();
			this.collectedSpin.Add ("name", "collectedSpin");
			this.collectedSpin.Add ("amount", new Vector3 (this.transform.position.x, 2880f, this.transform.position.z));
			this.collectedSpin.Add ("time", 2f);
			this.collectedSpin.Add ("delay", 1f);
			this.collectedSpin.Add ("easetype", iTween.EaseType.linear);
			this.collectedSpin.Add ("looptype", iTween.LoopType.none);
			
			// initialise the collectedmove hashtable
			this.collectedMove = new Hashtable ();
			this.collectedMove.Add ("name", "collectedMove");
			this.collectedMove.Add ("position", new Vector3 (this.transform.position.x, 3f, this.transform.position.z));
			this.collectedMove.Add ("time", 1.5f);
			
			// initialise the collectedshrink hashtable
			this.collectedShrink = new Hashtable ();
			this.collectedShrink.Add ("name", "collectedShrink");
			this.collectedShrink.Add ("scale", new Vector3 (0f, 0f, 0f));
			this.collectedShrink.Add ("time", 2f);
			this.collectedShrink.Add ("delay", 1.5f);
		}
		
		private void initMissedHashTables ()
		{
			// intialise the missedspin hashtable
			this.missedSpin = new Hashtable ();
			this.missedSpin.Add ("name", "missedSpin");
			this.missedSpin.Add ("amount", new Vector3 (this.transform.position.x, 2880f, this.transform.position.z));
			this.missedSpin.Add ("time", 2f);
			this.missedSpin.Add ("delay", 0f);
			this.missedSpin.Add ("easetype", iTween.EaseType.easeInCubic);
			this.missedSpin.Add ("looptype", iTween.LoopType.none);
		}
		
		void OnTriggerEnter (Collider other)
		{
			// if the player collides with the powerup
			if ("Player".Equals (other.transform.tag)) {
				StopCoroutine (this.missedPU ());
				this.audioSource.clip = this.pickupSound;
				this.audioSource.Play ();
				StartCoroutine (this.collectPU ());
			}
		}
		
		IEnumerator collectPU ()
		{	
			iTween.MoveTo (this.gameObject, this.collectedMove);			
			iTween.RotateAdd (this.gameObject, this.collectedSpin);
			iTween.ScaleTo (this.gameObject, this.collectedShrink);
			yield return new WaitForSeconds (5f);
			EventManager.TriggerEvent ("PlayerStateChange");
			gameObject.SetActive (false);
		}
		
		IEnumerator missedPU ()
		{
			// the player failed to pick up the power up in time, remove it from the scene
			this.sphereCollider = (SphereCollider)this.GetComponent (typeof(SphereCollider));
			this.sphereCollider.isTrigger = true;
			yield return new WaitForSeconds (8f);
			this.sphereCollider.isTrigger = false;
			iTween.RotateAdd (this.gameObject, this.missedSpin);
			iTween.ScaleTo (this.gameObject, this.collectedShrink);
			yield return new WaitForSeconds (2f);
			gameObject.SetActive (false);
		}
	}
}