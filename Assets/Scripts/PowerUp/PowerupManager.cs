using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace LightsOff
{
	public class PowerupManager : MonoBehaviour
	{
		public LevelManager levelManager;
		public GameObject parent;
		private int maxPowerUpCount;

		void OnEnable ()
		{
			EventManager.StartListening ("SpawnPowerUp", spawnNewPU);
		}

		void OnDisable ()
		{
			EventManager.StopListening ("SpawnPowerUp", spawnNewPU);
		}
		
		void spawnNewPU ()
		{
			int currentLightsOn = InGamePlayerPrefs.GetInt ("currentLightsOn");
			int currentLives = InGamePlayerPrefs.GetInt ("currentLives");
			int currentGrid = InGamePlayerPrefs.GetInt ("currentGrid");

			if (currentLightsOn > 0 && currentLives > 0) {
				// We have to make sure the power doesnt spawn where there are any obstacles or where the cube is
				gridtile[,] level = this.levelManager.GridLevels [currentGrid] ["GRID"] as gridtile[,];
				Vector2 positionToSpawn = this.createPositionToSpawn ();
				float x = positionToSpawn.x;
				if (currentGrid > 1) {
					positionToSpawn.x += (this.levelManager.squareGridSize () + this.levelManager.getGridSpacing () * currentGrid);
				}
				while (level[(int) x, (int) positionToSpawn.y].isOccupiedByObject() || level[(int) x, (int) positionToSpawn.y].HasPortal) {
					positionToSpawn = this.createPositionToSpawn ();
					x = positionToSpawn.x;
					if (currentGrid > 1) {
						positionToSpawn.x += (this.levelManager.squareGridSize () + this.levelManager.getGridSpacing () * currentGrid);
					}
				}

				GameObject powerUp = ObjectPooler.GetPooledPowerUp (Random.Range (0, 4));
				powerUp.transform.position = new Vector3 (positionToSpawn.x, -0.5f, positionToSpawn.y);
				powerUp.transform.parent = this.parent.transform;
				powerUp.SetActive (true);
				iTween.ScaleFrom (powerUp, new Vector3 (0, 0, 0), 1f);
			}
		}
		
		private Vector2 createPositionToSpawn ()
		{
			int x = Random.Range (0, this.levelManager.squareGridSize ());
			int y = Random.Range (0, this.levelManager.squareGridSize ());
			return new Vector2 (x, y);
		}
	}
}